﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Class for Walls in the Maze
/// </summary>
public class Wall : MonoBehaviour {

    /// <summary>
    /// Dictionary with the open spaces around the Wall
    /// </summary>
    private Dictionary<Maze.Directions, bool> ClearForObject = new Dictionary<Maze.Directions, bool>();

    /// <summary>
    /// List of Maze.Directions intergers of open spaces
    /// </summary>
    private List<int> numbers = new List<int>();

    /// <summary>
    /// Public Init method for Constructor use because we have a MonoBehaviour class
    /// </summary>
    /// <param name="details">Dictionary with the open spaces around our Wall</param>
    /// <param name="spawnObject">The object oru wall is allowed to spawn</param>
    /// <param name="seed">The seed of our maze</param>
    /// <param name="canSpawn">is the wall allowed to spawn an object</param>
    public void Init(Dictionary<Maze.Directions, bool> details, GameObject spawnObject, int seed, bool canSpawn = false)
    {
        // Create a new System.Random with our given seed
        System.Random random = new System.Random(seed);

        // If we are not allowed to spawn return the method
        if (!canSpawn)
        {
            enabled = false;
            return;
        }

        // Save our details in a local reference for future use if we decide to
        ClearForObject = details;
    
        // If there is any open space around our Wall
        if (details.Any(x => (x.Value == true)))
        {
            // Loop through every dictionary value
            for (int i = 0; i < details.Count; i++)
                
                // If the space is open
                if (details[(Maze.Directions)i] == true)

                    // Add this index to the numbers List
                    numbers.Add(i);

            // Get a randomLocation out of the numbers List
            int randomLocation = numbers[random.Next(numbers.Count)];

            // Instantiate the object
            GameObject spawnedObject = Instantiate(spawnObject);

            // Switch for our randomly selected object location
            switch ((Maze.Directions)randomLocation)
            {
                case Maze.Directions.North:
                    // Set the location to the North of our Wall and rotate towards it
                    spawnedObject.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.7f);
                    spawnedObject.transform.eulerAngles = new Vector3(0, 0, 0);
                    break;

                case Maze.Directions.East:
                    // Set the location to the East of our Wall and rotate towards it
                    spawnedObject.transform.position = new Vector3(transform.position.x + 0.7f, transform.position.y, transform.position.z);
                    spawnedObject.transform.eulerAngles = new Vector3(0, 90, 0);
                    break;

                case Maze.Directions.South:
                    // Set the location to the South of our Wall and rotate towards it
                    spawnedObject.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.7f);
                    spawnedObject.transform.eulerAngles = new Vector3(0, 180, 0);
                    break;

                case Maze.Directions.West:
                    // Set the location to the West of our Wall and rotate towards it
                    spawnedObject.transform.position = new Vector3(transform.position.x - 0.7f, transform.position.y, transform.position.z);
                    spawnedObject.transform.eulerAngles = new Vector3(0, 270, 0);
                    break;

            }

            // Set the parent of our created object to this Wall
            spawnedObject.transform.SetParent(gameObject.transform);

            // Disable the Wall.cs script
            enabled = false;
        }
    }
}
