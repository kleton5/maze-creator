﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class used for Cells in Prims maze algortihm
/// </summary>
public class MazeCell
{
    // The gridposition in the 2D array
    public Vector2 gridPosition;

    // The type of space (0 = open, 1 = wall)
    public int type;

    /// <summary>
    /// Dictionary with the neighbours of this MazeCell using <see cref="Maze.Directions"/> enum and the MazeCell reference
    /// </summary>
    public Dictionary<Maze.Directions, MazeCell> neighbours = new Dictionary<Maze.Directions, MazeCell>();

    /// <summary>
    /// Constructor of the MazeCell
    /// </summary>
    /// <param name="x">X position in the 2D array</param>
    /// <param name="y">Y position in the 2D array</param>
    public MazeCell(int x, int y)
    {
        gridPosition = new Vector2(x, y);
    }
}
