﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// Class used for Maze generation
/// </summary>
public class Maze : MonoBehaviour
{
    // Enum of possible Directions
    public enum Directions { North, East, South, West };

    // Delegate for Generated event
    public delegate void Generated();

    // Event fired OnGenerate for subscribing in other classes
    public event Generated OnGenerate;

    // Delegate for Build event
    public delegate void Build();

    // Event fired OnBuild for subscribing in other classes
    public event Build OnBuild;

    // Property to get the MazeWidth out of the UI element
    public int MazeWidth
    {
        get
        {
            // Minimal size of the mazeWidth
            int minSize = 10;

            if (widthInput.text == string.Empty)
                return minSize;

            if (int.Parse(widthInput.text) < 10)
                return minSize;

            return int.Parse(widthInput.text);
        }
    }

    // Property to get the MazeHeight out of the UI element
    public int MazeHeight
    {
        get
        {
            // Minimal size of the mazeWidth
            int minSize = 10;

            if (heightInput.text == string.Empty)
                return minSize;

            if (int.Parse(heightInput.text) < 10)
                return minSize;

            return int.Parse(heightInput.text);
        }
    }

    // Property to get the Seed out of the UI element
    public int Seed
    {
        get
        {
            // If the UI element is empty generate a random seed
            if (string.IsNullOrEmpty(seedInput.text))
                return new System.Random().Next(0, 99999);
            else
                return int.Parse(seedInput.text);
        }
    }

    /// <summary>
    /// Property for the center point of the maze
    /// </summary>
    public Vector3 CenterPoint { get; set; }

    // The mazeValues currently used
    private int[,] mazeValues;

    // Instance of the System.Random
    private System.Random random;

    // List of all roof objects
    private List<GameObject> roof = new List<GameObject>();

    // The objects we use to visualise our maze
    [SerializeField]
    private GameObject[] block, floor, roofs, wallObjects;

    // The objects we use to populize our created maze
    [Space(5)]
    [SerializeField]
    private GameObject player, enemy, finish;

    // The UI elements which we use for the maze generation
    [SerializeField]
    private InputField widthInput, heightInput, seedInput, animationInput;

    // The toggle of whether or not the animation of our maze generation should be played
    [SerializeField]
    private Toggle animationToggle;

    // A text field where we output our created Seed for the maze
    [SerializeField]
    private Text generateOutput;

    // The Dropdown field where the user can select which maze Algorithm to use
    [SerializeField]
    private Dropdown algorithmInput;

    [Space(5)]
    [SerializeField]
    [Tooltip("Steps between walls before an object spawns")]
    private int stepsBetweenObjects;

    [Space(20)]
    [SerializeField]
    [Tooltip("The Color of the current block the algorithm is looking at")]
    private Color mazeBuildingHeadColor;

    [SerializeField]
    [Tooltip("The Material of the current block the algorithm is looking at")]
    private Material mazeBuildingHeadMaterial;

    /// <summary>
    /// Dictionary of <see cref="Algorithm"/>
    /// </summary>
    private Dictionary<int, Algorithm> algorithms = new Dictionary<int, Algorithm>();

    // The selected Algorithm in the dropdown
    private Algorithm selectedAlgorithm;

    /// <summary>
    /// Run Initial Maze
    /// </summary>
    private void Start()
    {
        Generate();
    }

    /// <summary>
    /// Method called on Generate button press
    /// </summary>
    public void Generate()
    {
        // Stop the enemy movement and previous generation
        StopAllCoroutines();

        // Create a new System.Random with the Seed
        random = new System.Random(Seed);

        // Clear the algorithms dictionary
        algorithms.Clear();

        // Add the DepthFirst algorithm
        algorithms.Add(0, new DepthFirst(random));

        // Add the Prim algorithm
        algorithms.Add(1, new Prim(random));

        algorithms.Add(2, new BacterialGrowth(random));

        // Disable the enemy
        if (enemyObject)
            enemyObject.gameObject.SetActive(false);

        // Destroy the previously created maze
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        // Destroy the previously created roof
        for (int i = 0; i < roof.Count; i++)
            Destroy(roof[i]);
        roof.Clear();

        // Display the Seed used for the maze generation
        generateOutput.text = Seed.ToString();

        // Set the selectedAlgorithm to the selected dropdown value
        selectedAlgorithm = algorithms[algorithmInput.value];

        // Set mazeValues to the selectedAlgirthm.CreateMaze method
        mazeValues = selectedAlgorithm.CreateMaze(MazeWidth, MazeHeight);

        // Build the maze using the BuildMaze Method
        StartCoroutine(BuildMaze());

        // Calculate the CenterPoint 
        foreach (Transform child in transform)
            CenterPoint += child.transform.position;
        CenterPoint /= transform.childCount;

        // Fire the OnGenerate Event
        OnGenerate();
    }

    /// <summary>
    /// Method for toggling the roof
    /// </summary>
    public void ToggleRoof()
    {
        // Reverse the activeSelf for every roof
        for (int i = 0; i < roof.Count; i++)
            roof[i].SetActive(!roof[i].activeSelf);
    }

    /// <summary>
    /// Method for instantiating the player
    /// </summary>
    private void SetPlayer()
    {
        // Set the player position to the selectedAlgorithm width and heigth
        Instantiate(player, new Vector3(selectedAlgorithm.startPointWidth, 0, selectedAlgorithm.startPointHeight), Quaternion.identity).transform.SetParent(gameObject.transform);
    }

    // Keep track of enemy object
    private Enemy enemyObject;

    /// <summary>
    /// Method for instantiating the enemies
    /// </summary>
    /// <param name="count">The amount of enemies to instantiate</param>
    private void SetEnemy()
    {
            // Get a random location
            Vector2 r = RandomLocation();

        if (enemyObject == null)
        {
            // Instantiate the enemy on the random location
            enemyObject = Instantiate(enemy, new Vector3(r.x, 0, r.y), Quaternion.identity).GetComponent<Enemy>();
        }
        else
        {
            // Reuse the same enemy by putting him on his new position and enabling him again
            enemyObject.transform.position = new Vector3(r.x, 0, r.y);
            enemyObject.gameObject.SetActive(true);

            // Let the enemy find a new path
            enemyObject.FindPath();
        }
    }

    /// <summary>
    /// Method for generating a random location
    /// </summary>
    /// <returns>A Vector2 with the randomly generated location</returns>
    public Vector2 RandomLocation()
    {
        // Get a random width and height from the maze sizes
        int width = random.Next(mazeValues.GetLength(0) - 1);
        int height = random.Next(mazeValues.GetLength(1) - 1);

        // If the location is a wall do the Method again
        if (mazeValues[width, height] == 1)
            return RandomLocation();

        return new Vector2(width, height);
    }

    /// <summary>
    /// Method for building the maze
    /// </summary>
    private IEnumerator BuildMaze()
    {
        GameObject spawnedObject;

        // Create an array to keep track of wall locations
        GameObject[,] gameObjects = new GameObject[mazeValues.GetLength(0) - 1, mazeValues.GetLength(1) - 1];

        // Create the maze as a full block of walls
        for (int x = 0; x < mazeValues.GetLength(0) - 1; x++)
            for (int y = 0; y < mazeValues.GetLength(1) - 1; y++)
            {
                // Create a new random wall object
                spawnedObject = Instantiate(block[random.Next(block.Length)], new Vector3(x, 0, y), Quaternion.identity);

                // Set the wall to be a child of our GameObject
                spawnedObject.transform.SetParent(transform);

                // Set the wall in our array of walls
                gameObjects[x, y] = spawnedObject;
                spawnedObject.name = "(" + x + "," + y + ")";
            }

        // Keep track of the previous positions
        List<Vector3> previousLocations = new List<Vector3>();

        // While we still have vector3s in our selectedAlgorithm.buildOrder
        while (true)
        {
            // If the previousLocation has values in it
            if (previousLocations.Count != 0)
            {
                // Loop through each of the previousLocations
                foreach (Vector3 previousLocation in previousLocations)
                {
                    // Destroy the wall because it is part of the open spaces 
                    Destroy(gameObjects[(int)previousLocation.x, (int)previousLocation.y]);
                }
                // Clear the previousLocation list
                previousLocations.Clear();

                // Check if building is finished and if so break the while
                if (selectedAlgorithm.buildOrder.Count <= 0)
                    break;
            }

            // Keep a list of our current positions
            List<Vector3> locations = new List<Vector3>();

            // Get the current wall out of our buildOrder Queue
            Vector3 newLocation = selectedAlgorithm.buildOrder.Dequeue();
            
            // Add our currentWall to the locations to build
            locations.Add(newLocation);

            // If the newLocation.z is not equal to 0 which means it has a set index
            if(newLocation.z != 0)
                // While the next vector3 in our selectedAlgorithm.buildOrder z is equal to our current
                while (selectedAlgorithm.buildOrder.Count > 0 && selectedAlgorithm.buildOrder.Peek().z == newLocation.z)
                        // Add this location as well to our locations list
                        locations.Add(selectedAlgorithm.buildOrder.Dequeue());

            // For every location we added to our locations list
            foreach (Vector3 location in locations)
            {
                // Change the color of this wall to red to show what our algorithm is doing
                gameObjects[(int)location.x, (int)location.y].GetComponent<Renderer>().material = mazeBuildingHeadMaterial;
                mazeBuildingHeadMaterial.color = mazeBuildingHeadColor;

                // if the location is the finishPosition created the finish
                if ((int)location.x == selectedAlgorithm.finishPosition.x && (int)location.y == selectedAlgorithm.finishPosition.y)
                    spawnedObject = Instantiate(finish, new Vector3((int)location.x, -1, (int)location.y), Quaternion.identity);

                // otherwise create a floor object
                else
                    spawnedObject = Instantiate(floor[random.Next(floor.Length)], new Vector3((int)location.x, -1, (int)location.y), Quaternion.identity);

                // Set the newly created object parent to our current gameObject
                spawnedObject.transform.SetParent(transform);

                // If the animationToggle is enabled add the delay here
                if (animationToggle.isOn)
                    yield return new WaitForSeconds(float.Parse(animationInput.text));

                // Save the previous locations
                previousLocations.Add(location);
            }
        }

        // keep track of the amount of walls
        int spawningCount = 0;

        // Run over the entire maze and fill in the wallObjects
        for (int x = 0; x < mazeValues.GetLength(0) - 1; x++)
            for (int y = 0; y < mazeValues.GetLength(1) - 1; y++)
            {
                // If the wall can spawn an object based on the stepsBetweenObjects
                bool canSpawn = spawningCount >= stepsBetweenObjects ? true : false;
                if (canSpawn)
                    spawningCount = 0;

                // Add one to our counter of walls passed
                    spawningCount++;

                // Set the Wall object values
                if (gameObjects[x, y] != null) 
                    gameObjects[x,y].GetComponent<Wall>().Init(WallDetails(x, y), wallObjects[random.Next(wallObjects.Length)], Seed, canSpawn);

                // Instantiate the a roof object
                GameObject roofObject = Instantiate(roofs[random.Next(roofs.Length)], new Vector3(x, 1, y), Quaternion.identity);

                // Disable the roof object
                roofObject.SetActive(false);
                roofObject.transform.SetParent(gameObject.transform);

                // Add the roofobject to the roof List
                roof.Add(roofObject);
            }

        // Instantiate the Player
        SetPlayer();

        // Instantiate the enemy
        SetEnemy();

        // Fire the OnBuild event
        if(OnBuild != null)
         OnBuild();

        yield break;
    }

    /// <summary>
    /// Method for receiving Wall Details of a specific grid location
    /// </summary>
    /// <param name="width">The X position in the grid</param>
    /// <param name="height">The Y position in the grid</param>
    /// <returns>Returns a Dictionary with the directions and whether its open or not</returns>
    private Dictionary<Directions, bool> WallDetails(int width, int height)
    {
        // Create a new Dictionary
        Dictionary<Directions, bool> details = new Dictionary<Directions, bool>();

        // Create an array of all Directions
        var enumValues = System.Enum.GetValues(typeof(Directions)).Cast<Directions>().ToList();

        // Set every value in the Dictionary to false
        for (int i = 0; i < enumValues.Count; i++)
            details[enumValues[i]] = false;

        // If the given height + 1 fits within the maze and its an open space
        if (height + 1 < mazeValues.GetLength(1) - 1 && mazeValues[width, height + 1] == 0)
            details[Directions.North] = true;

        // If the given width + 1 fits within the maze and its an open space
        if (width + 1 < mazeValues.GetLength(0) - 1 && mazeValues[width + 1, height] == 0)
            details[Directions.East] = true;

        // If the given height - 1 fits within the maze and its an open space
        if (height - 1 > 0 && mazeValues[width, height - 1] == 0)
            details[Directions.South] = true;

        // If the given width - 1 fits within the maze and its an open space
        if (width - 1 > 0 && mazeValues[width - 1, height] == 0)
            details[Directions.West] = true;

        return details;
    }
}
