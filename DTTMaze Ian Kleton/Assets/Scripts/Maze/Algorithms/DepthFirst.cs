﻿using UnityEngine;
using System.Linq;

/// <summary>
/// Class for the DepthFirst Search Maze algorithm implementation
/// Inherits from <see cref="Algorithm"/>
/// </summary>
public class DepthFirst : Algorithm
{
    // The width of the maze
    private int mazeWidth;

    // The height of the maze
    private int mazeHeight;

    // Keep track of our index for the BuildOrderQueue so that we can display our Algorithms actions
    private int index = 1;

    /// <summary>
    /// Constructor method for DepthFirst class calling the <see cref="Algorithm.Algorithm(System.Random)"/>
    /// </summary>
    /// <param name="r">The System.Random Instance</param>
    public DepthFirst(System.Random r) : base(r)
    {
    }

    /// <summary>
    /// The Abstract <see cref="Algorithm.CreateMaze(int, int)"/> method implementation
    /// </summary>
    /// <param name="width">The maze width</param>
    /// <param name="height">The maze Height</param>
    /// <returns>Returns the completed maze as an int [,]</returns>
    public override int[,] CreateMaze(int width, int height)
    {
        // Create a [,] with the given size
        int[,] newMaze = new int[width, height];

        // Save the width and height locally in the class
        mazeWidth = width;
        mazeHeight = height;

        // Set all values in the maze to a wall
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                newMaze[i, j] = 1;

        // Get a random startPointWidth for DepthFirst search algorithm
        startPointWidth = random.Next(width - 4) + 2;
        while (startPointWidth % 2 == 0)
            startPointWidth = random.Next(width - 4) + 2;

        // Get a random startPointHeight for Depthfirst search algorithm
        startPointHeight = random.Next(height - 4) + 2;
        while (startPointHeight % 2 == 0)
            startPointHeight = random.Next(height - 4) + 2;

        // Set the start of the maze to an open field
        newMaze[startPointWidth, startPointHeight] = 0;

        // Call the DepthFirst Search algorithm
        MazeAlgorythm(newMaze, startPointWidth, startPointHeight);

        return newMaze;
    }

    /// <summary>
    /// Method for DepthFirst Search algorithm execution
    /// </summary>
    /// <param name="maze">The maze values</param>
    /// <param name="startWidth">The currentWidth</param>
    /// <param name="startHeight">The currentHeight</param>
    private void MazeAlgorythm(int[,] maze, int startWidth, int startHeight)
    {
        // Add this position to our order of actions
        buildOrder.Enqueue(new Vector3(startWidth, startHeight, index));

        // Set the finishPosition to the current Start and Width
        finishPosition = new Vector2(startWidth, startHeight);

        // Create an array of all possible direction in the Maze.Directions enum
        var directions = System.Enum.GetValues(typeof(Maze.Directions)).Cast<Maze.Directions>().ToArray();

        // Call the Shuffle method to rearrange the Directions randomly
        Shuffle(directions);

        for (int i = 0; i < directions.Length; i++)
        {
            // Switch over every possible direction
            switch (directions[i])
            {
                case Maze.Directions.North:
                    
                    // If the startHeight -2 is smaller than the grid
                    if (startHeight - 2 <= 0)
                        continue;

                    // If this point in the maze is still a wall
                    if (maze[startWidth, startHeight - 2] != 0)
                    {
                        // Open up this and the way towards this point in the maze
                        maze[startWidth, startHeight - 2] = 0;
                        maze[startWidth, startHeight - 1] = 0;

                        // Add these spaces to our order of actions
                        buildOrder.Enqueue(new Vector3(startWidth, startHeight - 1, index));
                        buildOrder.Enqueue(new Vector3(startWidth, startHeight - 2, index));

                        // Run the DepthFirst Search algorithm starting at our newly created open point
                        MazeAlgorythm(maze, startWidth, startHeight - 2);
                    }
                    break;

                case Maze.Directions.East:

                    // If the startWidth + 2 is greater than the grid
                    if (startWidth + 2 >= mazeWidth - 2)
                        continue;

                    // If this point in the maze is still a wall
                    if (maze[startWidth + 2, startHeight] != 0)
                    {
                        // Open up this and the way towards this point in the maze
                        maze[startWidth + 2, startHeight] = 0;
                        maze[startWidth + 1, startHeight] = 0;

                        // Add these spaces to our order of actions
                        buildOrder.Enqueue(new Vector3(startWidth + 1, startHeight, index));
                        buildOrder.Enqueue(new Vector3(startWidth + 2, startHeight, index));

                        // Run the DepthFirst Search algorithm starting at our newly created open point
                        MazeAlgorythm(maze, startWidth + 2, startHeight);
                    }
                    break;

                case Maze.Directions.South:

                    // If the startHeight + 2 is greater than the grid
                    if (startHeight + 2 >= mazeHeight - 2)
                        continue;

                    // If this point in the maze is still a wall
                    if (maze[startWidth, startHeight + 2] != 0)
                    {
                        // Open up this and the way towards this point in the maze
                        maze[startWidth, startHeight + 2] = 0;
                        maze[startWidth, startHeight + 1] = 0;

                        // Add these spaces to our order of actions
                        buildOrder.Enqueue(new Vector3(startWidth, startHeight + 1, index));
                        buildOrder.Enqueue(new Vector3(startWidth, startHeight + 2, index));

                        // Run the DepthFirst Search algorithm starting at our newly created open point
                        MazeAlgorythm(maze, startWidth, startHeight + 2);
                    }
                    break;

                case Maze.Directions.West:

                    // If the startWidth - 2 is smaller than the grid
                    if (startWidth - 2 <= 0)
                        continue;

                    // If this point in the maze is still a wall
                    if (maze[startWidth - 2, startHeight] != 0)
                    {
                        // Open up this and the way towards this point in the maze
                        maze[startWidth - 2, startHeight] = 0;
                        maze[startWidth - 1, startHeight] = 0;

                        // Add these spaces to our order of actions
                        buildOrder.Enqueue(new Vector3(startWidth - 1, startHeight, index));
                        buildOrder.Enqueue(new Vector3(startWidth - 2, startHeight, index));

                        // Run the DepthFirst Search algorithm starting at our newly created open point
                        MazeAlgorythm(maze, startWidth - 2, startHeight);
                    }
                    break;
            }
        }
        // Increase our index as we have finished the current interaction of the Algorithm
        index++;
    }

    /// <summary>
    /// Fisher Yates shuffle method to reorganise the array in the algorithm
    /// </summary>
    /// <typeparam name="T">Any type</typeparam>
    /// <param name="array">The array to shuffle</param>
    private void Shuffle<T>(T[] array)
    {
        // For every element in the array
        for (int i = array.Length; i > 1; i--)
        {
            // Get a random index out of the array
            int element = random.Next(array.Length);

            // Save the item at the randomIndex
            T tempItem = array[element];

            // Swap the items
            array[element] = array[i - 1];
            array[i - 1] = tempItem;
        }
    }
}
