﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Prim algorithm alteration by visiting all cells at the same time
/// </summary>
public class BacterialGrowth : Algorithm {

    /// <summary>
    /// Constructor method to call <see cref="Algorithm.Algorithm(System.Random)"/> Constructor method
    /// </summary>
    /// <param name="r"></param>
    public BacterialGrowth(System.Random r) : base(r)
    {
    }

    /// <summary>
    /// Override CreateMaze abstract method in <see cref="Algorithm.CreateMaze(int, int)"/> for maze generation
    /// </summary>
    /// <param name="width">The maze width</param>
    /// <param name="height">The maze height</param>
    /// <returns></returns>
    public override int[,] CreateMaze(int width, int height)
    {
        MazeCell[,] newMaze = new MazeCell[width, height];

        // Create a new MazeCell for every point in the maze
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
            {
                // Set the MazeCell gridValues
                MazeCell cell = new MazeCell(i, j);

                // Set the MazeCell to a wall
                cell.type = 1;

                // Reference the MazeCell in the maze
                newMaze[i, j] = cell;
            }

        // Get a random startPointWidth for Prim algorithm
        startPointWidth = random.Next(width - 4) + 2;
        while (startPointWidth % 2 == 0)
            startPointWidth = random.Next(width - 4) + 2;

        // Get a random startPointHeight for Prim algorithm
        startPointHeight = random.Next(height - 4) + 2;
        while (startPointHeight % 2 == 0)
            startPointHeight = random.Next(height - 4) + 2;

        // Run Prims maze algorithm at the startPoint of the maze
        RecursiveBacktrackerAlgorithm(newMaze, startPointWidth, startPointHeight);

        //Create a new int[,] array with the width and height of the maze
        int[,] finishedMaze = new int[width, height];

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                // Set the newMaze type into the finishedMaze to transfer the MazeCell maze into an int[,] maze
                finishedMaze[i, j] = newMaze[i, j].type;

        return finishedMaze;
    }

    public void RecursiveBacktrackerAlgorithm(MazeCell[,] maze, int width, int height)
    {
        // Create a List of MazeCells that we want to visit
        List<MazeCell> cells = new List<MazeCell>();

        // Create a List of MazeCells that we have already visited
        List<MazeCell> visited = new List<MazeCell>();

        // Add the starting Cell of our maze to the cells
        cells.Add(maze[width, height]);

        // Put our starting Cell as a visited Cell
        visited.Add(maze[width, height]);

        // Add our initial point to our Queue of actions
        buildOrder.Enqueue(new Vector3(width, height, 0));
        
        // Keep track of how many times we loop through the cells
        int cellsLooped = 0;

        // While we still have Cells to visit
        while (cells.Count != 0)
        {
            // Add one to the looped cells
            cellsLooped++;

            // Copy our cells to a new list
            List<MazeCell> loopCells = cells;

            // Randomize the new list
            Shuffle(loopCells);

            // Loop through all celsl in our new list
            for (int i = 0; i < loopCells.Count; i++)
            {
                // Create a reference to shorten code below
                MazeCell currentCell = loopCells[i];

                // Clear the neighbours of our current Cell
                currentCell.neighbours.Clear();

                // Check if our currentCell x position - 2 fits within the grid and it is not already in the Visited Cells List
                if (currentCell.gridPosition.x - 2 >= 0 && !visited.Contains(maze[(int)currentCell.gridPosition.x - 2, (int)currentCell.gridPosition.y]))

                    // Check if the neighbours of our currentCell doesnt already have a West neighbour
                    if (!currentCell.neighbours.ContainsKey(Maze.Directions.West))

                        // Add the neighbour to our currentCell neighbours
                        currentCell.neighbours.Add(Maze.Directions.West, maze[(int)currentCell.gridPosition.x - 2, (int)currentCell.gridPosition.y]);

                // Check if our currentCell x position + 2 fits within the grid and it is not already in the Visited Cells List
                if (currentCell.gridPosition.x + 2 < maze.GetLength(0) - 2 && !visited.Contains(maze[(int)currentCell.gridPosition.x + 2, (int)currentCell.gridPosition.y]))

                    // Check if the neighbours of our currentCell doesnt already have an East neighbour
                    if (!currentCell.neighbours.ContainsKey(Maze.Directions.East))

                        // Add the neighbour to our currentCell neighbours
                        currentCell.neighbours.Add(Maze.Directions.East, maze[(int)currentCell.gridPosition.x + 2, (int)currentCell.gridPosition.y]);

                // Check if our currentCell y position - 2 fits within the grid and it is not already in the Visited Cells List
                if (currentCell.gridPosition.y - 2 >= 0 && !visited.Contains(maze[(int)currentCell.gridPosition.x, (int)currentCell.gridPosition.y - 2]))

                    // Check if the neighbours of our currentCell doesnt already have an South neighbour
                    if (!currentCell.neighbours.ContainsKey(Maze.Directions.South))

                        // Add the neighbour to our currentCell neighbours
                        currentCell.neighbours.Add(Maze.Directions.South, maze[(int)currentCell.gridPosition.x, (int)currentCell.gridPosition.y - 2]);

                // Check if our currentCell y position + 2 fits within the grid and it is not already in the Visited Cells List
                if (currentCell.gridPosition.y + 2 < maze.GetLength(1) - 2 && !visited.Contains(maze[(int)currentCell.gridPosition.x, (int)currentCell.gridPosition.y + 2]))

                    // Check if the neighbours of our currentCell doesnt already have an North neighbour
                    if (!currentCell.neighbours.ContainsKey(Maze.Directions.North))

                        // Add the neighbour to our currentCell neighbours
                        currentCell.neighbours.Add(Maze.Directions.North, maze[(int)currentCell.gridPosition.x, (int)currentCell.gridPosition.y + 2]);

                // If our currentCell doesnt have any neighbours
                if (currentCell.neighbours.Count == 0)
                    // remove it from the Cells List
                    cells.RemoveAt(0);

                // If our currentCell has neighbours
                else
                {
                    // Create a List of intergers
                    List<int> numbers = new List<int>();

                    // For every value in the currentCell neighbours
                    foreach (KeyValuePair<Maze.Directions, MazeCell> n in currentCell.neighbours)
                    {
                        // If the MazeCell is not equal to null
                        if (n.Value != null)

                            // Add this Maze.Directions cast to int to our numbers List
                            numbers.Add((int)n.Key);
                    }

                    // Get a random neigbhour index from the numbers List
                    int randomNeighbour = numbers[random.Next(numbers.Count)];

                    // Get the MazeCell that matches this randomNeighbour index
                    MazeCell neighbour = currentCell.neighbours[(Maze.Directions)randomNeighbour];


                    // Find the case that matches our Maze.Directions
                    Vector3 openPathLocation = Vector3.zero;
                    switch ((Maze.Directions)randomNeighbour)
                    {
                        case Maze.Directions.North:
                            // Open up the position North of our currentCell
                            maze[(int)currentCell.gridPosition.x, (int)currentCell.gridPosition.y + 1].type = 0;
                            openPathLocation = new Vector3(currentCell.gridPosition.x, currentCell.gridPosition.y + 1);
                            break;

                        case Maze.Directions.East:
                            // Open up the position East of our currentCell
                            maze[(int)currentCell.gridPosition.x + 1, (int)currentCell.gridPosition.y].type = 0;
                            openPathLocation = new Vector3(currentCell.gridPosition.x + 1, currentCell.gridPosition.y);
                            break;

                        case Maze.Directions.South:
                            // Open up the position South of our currentCell
                            maze[(int)currentCell.gridPosition.x, (int)currentCell.gridPosition.y - 1].type = 0;
                            openPathLocation = new Vector3(currentCell.gridPosition.x, currentCell.gridPosition.y - 1);
                            break;

                        case Maze.Directions.West:
                            // Open up the position West of our currentCell
                            maze[(int)currentCell.gridPosition.x - 1, (int)currentCell.gridPosition.y].type = 0;
                            openPathLocation = new Vector3(currentCell.gridPosition.x - 1, currentCell.gridPosition.y);
                            break;
                    }
                    // Set the finishPosition to this new open space
                    finishPosition = neighbour.gridPosition;

                    // Add the path we cleared to our Queue of actions
                    buildOrder.Enqueue(new Vector3(openPathLocation.x, openPathLocation.y, cellsLooped));

                    // Set our neighbourCell to an open space
                    maze[(int)neighbour.gridPosition.x, (int)neighbour.gridPosition.y].type = 0;

                    // Add our neighbourCell to the visited List
                    visited.Add(neighbour);

                    //Add our neighbourCell to our cells List
                    cells.Add(neighbour);

                    // Add our chosen neighbour to our Queue of actions
                    buildOrder.Enqueue(new Vector3(neighbour.gridPosition.x, neighbour.gridPosition.y, cellsLooped));
                }
            }
        }
    }

    /// <summary>
    /// Fisher Yates shuffle method to reorganise the list in the algorithm
    /// </summary>
    /// <typeparam name="T">Any type</typeparam>
    /// <param name="List">The List to shuffle</param>
    private void Shuffle<T>(List<T> list)
    {
        // For every element in the list
        for (int i = list.Count; i > 1; i--)
        {
            // Get a random index out of the list
            int element = random.Next(list.Count);

            // Save the item at the randomIndex
            T tempItem = list[element];

            // Swap the items
            list[element] = list[i - 1];
            list[i - 1] = tempItem;
        }
    }
}
