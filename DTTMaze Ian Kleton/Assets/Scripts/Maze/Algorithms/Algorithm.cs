﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Abstract class for Maze Algorithms
/// </summary>
public abstract class Algorithm
{
    // startWidth of the maze
    public int startPointWidth { get; set; }

    // startheight of the maze
    public int startPointHeight { get; set; }

    // position of the finish for the maze
    public Vector2 finishPosition { get; set; }

    // Queue of the order of actions in our algorithm, We use Vector3 so that we can use the Z value for the index of the BuildOrder
    public Queue<Vector3> buildOrder = new Queue<Vector3>();

    // Instance of the System.Random used in Maze.cs
    protected System.Random random;

    /// <summary>
    /// Public Abstract Method for Creating the Maze
    /// </summary>
    /// <param name="width">Width of the maze</param>
    /// <param name="height">Height of the maze</param>
    /// <returns></returns>
    public abstract int[,] CreateMaze(int width, int height);

    /// <summary>
    /// Constructor of the Algorithm class
    /// </summary>
    /// <param name="r">Instance of the System.Random</param>
    public Algorithm(System.Random r)
    {
        random = r;
    }
}
