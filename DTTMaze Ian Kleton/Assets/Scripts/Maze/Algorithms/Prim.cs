﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for the Prim Maze algorithm implementation
/// Inherits from <see cref="Algorithm"/>
/// </summary>
public class Prim : Algorithm
{
    /// <summary>
    /// Constructor method for Prim class calling the <see cref="Algorithm.Algorithm(System.Random)"/>
    /// </summary>
    public Prim(System.Random r) : base(r)
    {
    }

    /// <summary>
    /// The Abstract <see cref="Algorithm.CreateMaze(int, int)"/> method implementation
    /// </summary>
    /// <param name="width">The maze width</param>
    /// <param name="height">The maze Height</param>
    /// <returns>Returns the completed maze as an int [,]</returns>
    public override int[,] CreateMaze(int width, int height)
    {
        // Create a new MazeCell[,] with the given width and height
        MazeCell[,] newMaze = new MazeCell[width, height];

        // Create a new MazeCell for every point in the maze
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
            {
                // Set the MazeCell gridValues
                MazeCell cell = new MazeCell(i, j);

                // Set the MazeCell to a wall
                cell.type = 1;

                // Reference the MazeCell in the maze
                newMaze[i, j] = cell;
            }

        // Get a random startPointWidth for Prim algorithm
        startPointWidth = random.Next(width - 4) + 2;
        while (startPointWidth % 2 == 0)
            startPointWidth = random.Next(width - 4) + 2;

        // Get a random startPointHeight for Prim algorithm
        startPointHeight = random.Next(height - 4) + 2;
        while (startPointHeight % 2 == 0)
            startPointHeight = random.Next(height - 4) + 2;

        // Set the start of the maze to an open field
        newMaze[startPointWidth, startPointHeight].type = 0;

        // Run Prims maze algorithm at the startPoint of the maze
        PrimAlgorithm(newMaze, startPointWidth, startPointHeight);

        // Create a new int[,] array with the width and height of the maze
        int[,] finishedMaze = new int[width, height];

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                // Set the newMaze type into the finishedMaze to transfer the MazeCell maze into an int[,] maze
                finishedMaze[i, j] = newMaze[i, j].type;

        return finishedMaze;
    }

    /// <summary>
    /// Method for Prim maze algorithm execution
    /// </summary>
    /// <param name="maze">The MazeCell maze values</param>
    /// <param name="startWidth">The currentWidth</param>
    /// <param name="startHeight">The currentHeight</param>
    private void PrimAlgorithm(MazeCell[,] maze, int width, int height)
    {
        // Create a List of MazeCells that we want to visit
        List<MazeCell> cells = new List<MazeCell>();

        // Create a List of MazeCells that we have already visited
        List<MazeCell> visited = new List<MazeCell>();

        // Add the starting Cell of our maze to the cells
        cells.Add(maze[width, height]);

        // Put our starting Cell as a visited Cell
        visited.Add(maze[width, height]);

        // Add our initial point to our Queue of actions
        buildOrder.Enqueue(new Vector2(width, height));

        // While we still have Cells to visit
        while (cells.Count != 0)
        {
            // Get a random Cell out of the remaining Cells
            int randomIndex = random.Next(cells.Count);

            // Create a reference to shorten code below
            MazeCell prim = cells[randomIndex];
            // Clear the neighbours of our current Cell
            prim.neighbours.Clear();

            // Check if our currentCell x position - 2 fits within the grid and it is not already in the Visited Cells List
            if (prim.gridPosition.x - 2 >= 0 && !visited.Contains(maze[(int)prim.gridPosition.x - 2, (int)prim.gridPosition.y]))

                // Check if the neighbours of our currentCell doesnt already have a West neighbour
                if (!prim.neighbours.ContainsKey(Maze.Directions.West))

                    // Add the neighbour to our currentCell neighbours
                    prim.neighbours.Add(Maze.Directions.West, maze[(int)prim.gridPosition.x - 2, (int)prim.gridPosition.y]);

            // Check if our currentCell x position + 2 fits within the grid and it is not already in the Visited Cells List
            if (prim.gridPosition.x + 2 < maze.GetLength(0) - 2 && !visited.Contains(maze[(int)prim.gridPosition.x + 2, (int)prim.gridPosition.y]))

                // Check if the neighbours of our currentCell doesnt already have an East neighbour
                if (!prim.neighbours.ContainsKey(Maze.Directions.East))

                    // Add the neighbour to our currentCell neighbours
                    prim.neighbours.Add(Maze.Directions.East, maze[(int)prim.gridPosition.x + 2, (int)prim.gridPosition.y]);

            // Check if our currentCell y position - 2 fits within the grid and it is not already in the Visited Cells List
            if (prim.gridPosition.y - 2 >= 0 && !visited.Contains(maze[(int)prim.gridPosition.x, (int)prim.gridPosition.y - 2]))

                // Check if the neighbours of our currentCell doesnt already have an South neighbour
                if (!prim.neighbours.ContainsKey(Maze.Directions.South))

                    // Add the neighbour to our currentCell neighbours
                    prim.neighbours.Add(Maze.Directions.South, maze[(int)prim.gridPosition.x, (int)prim.gridPosition.y - 2]);

            // Check if our currentCell y position + 2 fits within the grid and it is not already in the Visited Cells List
            if (prim.gridPosition.y + 2 < maze.GetLength(1) - 2 && !visited.Contains(maze[(int)prim.gridPosition.x, (int)prim.gridPosition.y + 2]))

                // Check if the neighbours of our currentCell doesnt already have an North neighbour
                if (!prim.neighbours.ContainsKey(Maze.Directions.North))

                    // Add the neighbour to our currentCell neighbours
                    prim.neighbours.Add(Maze.Directions.North, maze[(int)prim.gridPosition.x, (int)prim.gridPosition.y + 2]);

            // If our currentCell doesnt have any neighbours
            if (prim.neighbours.Count == 0)

                // remove it from the Cells List
                cells.RemoveAt(0);

            // If our currentCell has neighbours
            else
            {
                // Create a List of intergers
                List<int> numbers = new List<int>();

                // For every value in the currentCell neighbours
                foreach (KeyValuePair<Maze.Directions, MazeCell> n in prim.neighbours)
                {
                    // If the MazeCell is not equal to null
                    if (n.Value != null)

                        // Add this Maze.Directions cast to int to our numbers List
                        numbers.Add((int)n.Key);
                }

                // Get a random neigbhour index from the numbers List
                int randomNeighbour = numbers[random.Next(numbers.Count)];

                // Get the MazeCell that matches this randomNeighbour index
                MazeCell neighbour = prim.neighbours[(Maze.Directions)randomNeighbour];


                // Find the case that matches our Maze.Directions
                Vector2 openPathLocation = Vector2.zero;
                switch ((Maze.Directions)randomNeighbour)
                {
                    case Maze.Directions.North:
                        // Open up the position North of our currentCell
                        maze[(int)prim.gridPosition.x, (int)prim.gridPosition.y + 1].type = 0;
                        openPathLocation = new Vector2(prim.gridPosition.x, prim.gridPosition.y + 1);
                        break;

                    case Maze.Directions.East:
                        // Open up the position East of our currentCell
                        maze[(int)prim.gridPosition.x + 1, (int)prim.gridPosition.y].type = 0;
                        openPathLocation = new Vector2(prim.gridPosition.x + 1, prim.gridPosition.y);
                        break;

                    case Maze.Directions.South:
                        // Open up the position South of our currentCell
                        maze[(int)prim.gridPosition.x, (int)prim.gridPosition.y - 1].type = 0;
                        openPathLocation = new Vector2(prim.gridPosition.x, prim.gridPosition.y - 1);
                        break;

                    case Maze.Directions.West:
                        // Open up the position West of our currentCell
                        maze[(int)prim.gridPosition.x - 1, (int)prim.gridPosition.y].type = 0;
                        openPathLocation = new Vector2(prim.gridPosition.x - 1, prim.gridPosition.y);
                        break;
                }
                // Set the finishPosition to this new open space
                finishPosition = neighbour.gridPosition;

                // Add the path we cleared to our Queue of actions
                buildOrder.Enqueue(openPathLocation);

                // Set our neighbourCell to an open space
                maze[(int)neighbour.gridPosition.x, (int)neighbour.gridPosition.y].type = 0;

                // Add our neighbourCell to the visited List
                visited.Add(neighbour);

                //Add our neighbourCell to our cells List
                cells.Add(neighbour);

                // Add our chosen neighbour to our Queue of actions
                buildOrder.Enqueue(neighbour.gridPosition);
            }
        }
    }
}
