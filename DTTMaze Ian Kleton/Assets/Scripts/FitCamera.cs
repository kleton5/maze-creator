﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitCamera : MonoBehaviour {

    [SerializeField]
    [Tooltip("Size of the MouseBoundaries in which it moves the screen")]
    private int MouseBoundary;

    [SerializeField]
    [Tooltip("The Speed of the character movement")]
    private float MoveSpeed;

    [SerializeField]
    [Tooltip("The Speed at which the character turns in firstperson mode")]
    private float TurnSpeed;

    [SerializeField]
    [Tooltip("The Speed of which the camera zooms in with the scrollwheel")]
    private float ZoomSpeed;

    private Maze maze;

    // The current camera position
    private Vector3 cameraPosition;

    // Keep track of First person or topdown
    private bool pov;

    // Reference to the player transform position
    private Transform player;

    // The size of the screen
    private int screenWidth;
    private int screenHeight;

    // CameraChange event for use in the Enemy.cs class
    public delegate void CameraChange(bool state);
    public event CameraChange OnCameraChange;

    /// <summary>
    /// OnEnable method to add <see cref="Maze.OnGenerate"/> event to our <see cref="FitToScreen"/> Method
    /// </summary>
    private void OnEnable()
    {
        maze.OnGenerate += FitToScreen;
    }

    /// <summary>
    /// OnEnable method to remove <see cref="Maze.OnGenerate"/> event from our <see cref="FitToScreen"/> Method
    /// </summary>
    private void OnDisable()
    {
        maze.OnGenerate -= FitToScreen;
    }

    /// <summary>
    /// Awake method in which we gather the required data for our variables
    /// </summary>
    private void Awake()
    {
        maze = FindObjectOfType<Maze>();
        screenWidth = Screen.width;
        screenHeight = Screen.height;

        cameraPosition = transform.position;
    }

    /// <summary>
    /// Update method where we handle the scroll and camera positioning
    /// </summary>
    private void Update()
    {
        // Run MouseBoundaries method to handle the MouseBoundary movement
        MouseBoundaries();

        // If we are in topdown
        if (!pov)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                cameraPosition.y -= Time.deltaTime * ZoomSpeed;

            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
                cameraPosition.y += Time.deltaTime * ZoomSpeed;
        }

        // If we are in first person
        else
        {
            transform.position = player.transform.position;
            cameraPosition = transform.position;
        }

        if (transform.position != cameraPosition)
            transform.position = cameraPosition;
    }

    /// <summary>
    /// Method that handles the movement related to our MouseBoundaries
    /// </summary>
    private void MouseBoundaries()
    {
        // Save the current mouseposition
        Vector3 mousePosition = Input.mousePosition;

        // If we are in topdown mode
        if (!pov)
        {
            // Handle the movement if we are in the mouseboundaries
            if (mousePosition.x > screenWidth - MouseBoundary)
                cameraPosition.x += Time.deltaTime * MoveSpeed;

            else if (mousePosition.x < 0 + MouseBoundary)
                cameraPosition.x -= Time.deltaTime * MoveSpeed;

            if (mousePosition.y > screenHeight - MouseBoundary)
                cameraPosition.z += Time.deltaTime * MoveSpeed;

            else if (mousePosition.y < 0 + MouseBoundary)
                cameraPosition.z -= Time.deltaTime * MoveSpeed;
        }
       
        // If we are in firstperson mode
        else
        {
            // Handle the turning of the camera based on the mouseboundaries
            if (mousePosition.x > screenWidth - MouseBoundary)
                transform.eulerAngles +=  new Vector3(0, Time.deltaTime * TurnSpeed,0);

            else if (mousePosition.x < 0 + MouseBoundary)
                transform.eulerAngles -= new Vector3(0, Time.deltaTime * TurnSpeed, 0);
        }
    }

    /// <summary>
    /// Method called on <see cref="Maze.Generated"/> event to alter the camera view
    /// </summary>
    private void FitToScreen()
    {
        // Always go to TopDown mode first when we generate a new maze
        TopDown();

        // If our OnCameraChange method exists fire it giving our current camera state
        if(OnCameraChange != null)
            OnCameraChange(pov);
    }

    /// <summary>
    /// Method for changing the camera to TopDown mode
    /// </summary>
    private void TopDown()
    {
        // Disable the atmosphere audio
        GetComponent<AudioSource>().enabled = false;

        // Disable the fog
        RenderSettings.fog = false;

        // Unparent our camera to stop following our player
        transform.SetParent(null);

        // Change the angle to topdown
        transform.eulerAngles = new Vector3(90, 0, 0);

        // Set our bool to show that we are in top down
        pov = false;

        // Remove the clipping so that we can see all objects
        GetComponent<Camera>().farClipPlane = 1000;

        // Get the center point to recenter the camera
        transform.position = new Vector3(maze.CenterPoint.x, 50 + ((maze.MazeWidth + maze.MazeHeight) / 2.3f), maze.CenterPoint.z);
        cameraPosition = transform.position;
    }

    /// <summary>
    /// Method to change the camera mode to FirstPerson
    /// </summary>
    private void FirstPerson()
    {
        // Enable the atmosphere sound
        GetComponent<AudioSource>().enabled = true;

        // Enable the fog
        RenderSettings.fog = true;

        // Get the player transform
        player = FindObjectOfType<Player>().transform;

        // Set rotate our camera to first person
        transform.eulerAngles = new Vector3(0, 0, 0);

        // Reduce the clipping plane to only render walls we can actually see
        GetComponent<Camera>().farClipPlane = 10;

        // Set our bool to show that we are in first person
        pov = true;
    }

    /// <summary>
    /// Method for changing pov to the opposing mode
    /// </summary>
    public void ChangePOV()
    {
        // Toggle the roofs off
        FindObjectOfType<Maze>().ToggleRoof();

        // If we are in firstperson
        if (pov)
            TopDown();

        // If we are in topdown
        else
            FirstPerson();
        

        // Fire the OnCameraChange event with the current state of our camera
        OnCameraChange(pov);
    }
}
