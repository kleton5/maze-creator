﻿using System;

/// <summary>
/// Heap class for data sorting
/// </summary>
/// <typeparam name="T">Item to sort</typeparam>
public class Heap<T> where T : IHeapItem<T>
{
    // Array of items 
    T[] items;

    // Current count
    private int itemCount;

    // Property for requesting the itemCount
    public int ItemCount
    {
        get
        {
            return itemCount;
        }
    }

    /// <summary>
    /// Constructor of the Heap class
    /// </summary>
    /// <param name="maxHeapSize">The size of the Heap</param>
    public Heap(int maxHeapSize)
    {
        items = new T[maxHeapSize];
    }

    /// <summary>
    /// Method that adds an item to the Heap
    /// </summary>
    /// <param name="item">The item to add</param>
    public void Add(T item)
    {
        item.HeapIndex = itemCount;
        items[itemCount] = item;

        // Call sortup to resort the item array
        SortUp(item);
        itemCount++;
    }

    /// <summary>
    /// Method to update a specific item in the Heap
    /// </summary>
    /// <param name="item">The item to update in the Heap</param>
    public void UpdateItem(T item)
    {
        // Call sortup to resort the item given in the method
        SortUp(item);
    }

    /// <summary>
    /// Method to remove the first element in the Heap
    /// </summary>
    /// <returns>The removed item from the Heap</returns>
    public T RemoveFirst()
    {
        // Get the first item and save the reference
        T firstItem = items[0];
        itemCount--;

        // Set the first item in the Heap to the last item forcing a full resort
        items[0] = items[itemCount];
        items[0].HeapIndex = 0;
        SortDown(items[0]);

        return firstItem;
    }

    /// <summary>
    /// Method for checking if an item exists in the Heap
    /// </summary>
    /// <param name="item">The item to find</param>
    /// <returns></returns>
    public bool Contains(T item)
    {
        // Use equals interface method
        return Equals(items[item.HeapIndex], item);
    }

    /// <summary>
    /// Method for sorting up the Heap
    /// </summary>
    /// <param name="item">The item to sort up</param>
    private void SortUp(T item)
    {
        int parentIndex = (item.HeapIndex - 1) / 2;

        // Keep repeating till the Heap is done sorting
        while (true)
        {
            // Get the parent item in the Heap
            T parent = items[parentIndex];
            
            // If the current item contains a lower HeapIndex swap up
            if (item.CompareTo(parent) > 0)
                Swap(item, parent);

            else
                break;

            parentIndex = (item.HeapIndex - 1) / 2;
        }
    }

    /// <summary>
    /// Method for sorting down the Heap
    /// </summary>
    /// <param name="item">The item to sort down</param>
    private void SortDown(T item)
    {
        // Keep repeating until the Heap is done sorting
        while (true)
        {
            // Get both children of the Heap
            int childIndexLeft = item.HeapIndex * 2 + 1;
            int childIndexRight = item.HeapIndex * 2 + 2;

            int swapIndex = 0;

            // Check if the left child exists
            if (childIndexLeft < itemCount)
            {
                swapIndex = childIndexLeft;
                
                // Check if the right child exists
                if (childIndexRight < itemCount)
                {
                    // Use Icomparable interface method to compare the children and find the lowest value child
                    if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
                        swapIndex = childIndexRight;
                }

                // See if the parent requires to be swapped with the children
                if (item.CompareTo(items[swapIndex]) < 0)
                    Swap(item, items[swapIndex]);
                else
                    return;
            }
            else
                return;
        }
    }

    /// <summary>
    /// Method for swapping items in the Heap
    /// </summary>
    /// <param name="a">The first item</param>
    /// <param name="b">The second item</param>
    private void Swap(T a, T b)
    {
        // Swap the items in the Heap
        items[a.HeapIndex] = b;
        items[b.HeapIndex] = a;

        // Save a reference to A's HeapIndex
        int aIndex = a.HeapIndex;

        // Swap the Heap indexes of the items
        a.HeapIndex = b.HeapIndex;
        b.HeapIndex = aIndex;
    }
}

/// <summary>
/// HeapItem interface method for using IComparable method implementation
/// </summary>
/// <typeparam name="T">IComparable Item</typeparam>
public interface IHeapItem<T> : IComparable<T>
{
    // HeapIndex of this HeapItem
    int HeapIndex { get; set;}
}
