﻿using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Class that handles PathRequests for pathfinding
/// </summary>
public class PathRequest : MonoBehaviour
{
    // Queue of all Paths to be processed
    private Queue<Path> pathRequestQueue = new Queue<Path>();

    // The current Path thats being handled in the processing
    private Path currentPathRequest;

    // Reference to the Pathfinder.cs class
    private Pathfinder pathfinder;

    // Static instance of our current PathRequest class
    private static PathRequest instance;

    // Keep track of whether or not we are processsing a Path
    private bool isProcessing;

    /// <summary>
    /// Awake Method for setting initial values
    /// </summary>
    private void Awake()
    {
        // Set the instance to this class
        instance = this;

        // Get the reference to the Pathfinder.cs
        pathfinder = GetComponent<Pathfinder>();
    }

    /// <summary>
    /// Static Method for Requesting Paths throughout each class in our game
    /// </summary>
    /// <param name="pathStart">The start point of the path</param>
    /// <param name="pathEnd">The destination of our path</param>
    /// <param name="callback">Method to execute on RequestPath completion</param>
    public static void RequestPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
        // return if the start and end are equal
        if (pathStart == pathEnd)
            return;

        // Create a new Path with the given values
        Path newRequest = new Path(pathStart, pathEnd, callback);

        // Add the new Path to the requestQueue
        instance.pathRequestQueue.Enqueue(newRequest);

        // Attempt to process the next Path
        instance.TryProcessNext();
    }

    /// <summary>
    /// Method for processing Paths in the pathRequestQueue
    /// </summary>
    private void TryProcessNext()
    {
        // If there are Paths in the requestQueue and we are not processing
        if (!isProcessing && pathRequestQueue.Count > 0)
        {
            // Set our currentPathRequest to the next Path in the Queue
            currentPathRequest = pathRequestQueue.Dequeue();

            isProcessing = true;

            // Call the StartFindPath method in the Pathfinder.cs with our currentPathRequest
            pathfinder.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
        }
    }

    /// <summary>
    /// Public Method called when <see cref="Pathfinder.StartFindPath(Vector3, Vector3)"/> finishes the Path
    /// </summary>
    /// <param name="path"> The path from <see cref="Pathfinder.StartFindPath(Vector3, Vector3)"/></param>
    /// <param name="success">The state of <see cref="Pathfinder.StartFindPath(Vector3, Vector3)"/></param>
    public void FinishedProcessingPath(Vector3[] path, bool success)
    {
        // Send the path and state to the callback of the handled Path
        currentPathRequest.callback(path, success);

        // Complete isProcessing and process the next Path
        isProcessing = false;
        TryProcessNext();
    }

    /// <summary>
    /// Struct for Paths which we use in the <see cref="pathRequestQueue"/>
    /// </summary>
    struct Path
    {
        // The start of the path
        public Vector3 pathStart;

        // The destination of the path
        public Vector3 pathEnd;

        // Method executed when the path is found
        public Action<Vector3[], bool> callback;

        /// <summary>
        /// Constructor of the Path struct
        /// </summary>
        /// <param name="start">The start of the path</param>
        /// <param name="end">The destination of the path</param>
        /// <param name="callback">Method executed when the path is found</param>
        public Path(Vector3 start, Vector3 end, Action<Vector3[], bool> callback)
        {
            pathStart = start;
            pathEnd = end;
            this.callback = callback;
        }
    }
}
