﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Class used for enemy movement control
/// </summary>
public class Enemy : MonoBehaviour
{
    // Target to chase
    private Transform target;

    // The speed of which to chase with
    [SerializeField]
    [Tooltip("Speed of the enemy")]
    private float speed = 10;

    // Delay between pathfinding updates
    [SerializeField]
    [Tooltip("Speed at which we update our pathfinding path")]
    private float updatePathDelay = 3;

    // Different models for clarity in game
    [SerializeField]
    private GameObject topDownMesh, firstPersonMesh;

    // The waypoint path towards the target
    private Vector3[] path;

    // The current waypoint
    private int targetIndex;

    // Reference to the audiosource of the enemy
    private AudioSource audioSource;


    /// <summary>
    /// Start method in which we aquire the audiosource
    /// </summary>
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.enabled = false;
        FindPath();
    }

    /// <summary>
    /// Public method for requesting path
    /// </summary>
    public void FindPath()
    {
        StartCoroutine(RequestPath());
        StopCoroutine(FollowPath());
    }

    /// <summary>
    /// Add the receiver from the listener on enable
    /// </summary>
    private void OnEnable()
    {
        FindObjectOfType<FitCamera>().OnCameraChange += ChangeCamera;
    }

    /// <summary>
    /// Remove the receiver from the listener on disable
    /// </summary>
    private void OnDisable()
    {
        if(FindObjectOfType<FitCamera>())
            FindObjectOfType<FitCamera>().OnCameraChange -= ChangeCamera;
    }

    /// <summary>
    /// Method called everytime the camera changes viewmode
    /// </summary>
    private void ChangeCamera(bool pov)
    {
        if (audioSource == null)
            return;

        // Change the mesh based on the camera state
        if (pov)
        {
            audioSource.enabled = true;
            firstPersonMesh.SetActive(true);
            topDownMesh.SetActive(false);
        }
        else
        {
            audioSource.enabled = false;
            firstPersonMesh.SetActive(false);
            topDownMesh.SetActive(true);
        }
    }

    /// <summary>
    /// Continious loop to update path towards the target
    /// </summary>
    private IEnumerator RequestPath()
    {
        // Create a bool to see if we are running the path for the first time
        bool firstPath = true;
        while (true)
        {
            yield return new WaitForSeconds(0.1f);

            // find the target
            target = FindObjectOfType<Player>().transform;

            // Request a new path towards the target
            PathRequest.RequestPath(transform.position, target.position, OnPathFound);

            // If this is not the first path we add in our set delay
            if (!firstPath)
            {
                // use the updatePathDelays as a waiting time between path updates
                yield return new WaitForSeconds(updatePathDelay);
            }

            // If this is our first time we instantly want a path to be generated
            else
            {
                yield return new WaitForEndOfFrame();
                firstPath = false;
            }

            // Break method if target doesn't exist
            if (target == null)
                yield break;
        }
    }

    /// <summary>
    /// Method called when path is returned
    /// </summary>
    /// <param name="newPath"> The new path</param>
    /// <param name="succes"> The pathfinding result state</param>
    public void OnPathFound(Vector3[] newPath, bool succes)
    {
        // When a path is found towards the target
        if (succes)
        {
            // Update the path and reset the index
            path = newPath;
            targetIndex = 0;

           // Stop following the old path and refresh it with the new one
           StartCoroutine(FollowPath());
        }
    }

    /// <summary>
    /// Coroutine to follow a given path towards the target
    /// </summary>
    /// <returns></returns>
    private IEnumerator FollowPath()
    {
        // The path to follow
        Vector3[] currentPath = path;

        // Break coroutine if there is no path
        if (currentPath.Length == 0)
            yield break;

        // Set first waypoint
        Vector3 currentWayPoint = currentPath[0];

        while (true)
        {
            // Break if the path is updated
            if (currentPath != path)
                yield break;

            // If waypoint location is reached 
            if (transform.position == currentWayPoint)
            {
                // Increase the waypoint index
                targetIndex++;
                if (targetIndex >= currentPath.Length)
                    yield break;
                currentWayPoint = currentPath[targetIndex];
            }

            // Direction to look at
            Vector3 TargetDirection = currentWayPoint - transform.position;

            TargetDirection = new Vector3(TargetDirection.x, transform.position.y, TargetDirection.z);
            TargetDirection.Normalize();

            // Slowly rotate in the direction of the target waypoint
            TargetDirection = Vector3.Slerp(transform.forward, TargetDirection, Time.deltaTime * 9 /* The speed of which to rotate with */);
            TargetDirection += transform.position;
            transform.LookAt(TargetDirection);

            // Move towards the current waypoint using speed set in the inspector
            transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, speed * Time.deltaTime);
            yield return null;
        }
    }

    /// <summary>
    /// Draw Gizmos for showing the path
    /// </summary>
    private void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(path[i], Vector3.one * 0.4f);

                if (i == targetIndex)
                    Gizmos.DrawLine(transform.position, path[i]);
                else
                    Gizmos.DrawLine(path[i - 1], path[i]);
            }
        }
    }

    /// <summary>
    /// Method for collision detection
    /// </summary>
    /// <param name="other">The collision that was detected </param>
    private void OnCollisionStay(Collision other)
    {
        // If the collision was with a player
        if (other.gameObject.GetComponent<Player>())
        {
            // Get a new random location in the maze 
            Vector2 newPosition = FindObjectOfType<Maze>().RandomLocation();

            // Go to the new location and refresh the pathfinder
            transform.position = new Vector3(newPosition.x, 0, newPosition.y);
            target = FindObjectOfType<Player>().transform;
            PathRequest.RequestPath(transform.position, target.position, OnPathFound);
        }
    }
}
