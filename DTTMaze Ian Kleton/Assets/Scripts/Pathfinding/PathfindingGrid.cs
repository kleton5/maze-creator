﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for creating teh Pathfinding Node grid
/// </summary>
public class PathfindingGrid : MonoBehaviour {

    // The Node 2D array
    private Node[,] grid;

    // The size of the grid
    private Vector2 worldSize;

    [SerializeField]
    [Tooltip("Display the grid of Nodes for pathfinding")]
    private bool displayGridGizmos;

    [SerializeField]
    [Tooltip("The Layer on which pathfinding can't walk")]
    private LayerMask unwalkableLayer;

    [SerializeField]
    [Tooltip("The size of the Nodes")]
    private float nodeRadius;

    private float nodeDiameter;
    private int gridSizeX, gridSizeY;

    // bottomLeft location of the grid
    private Vector3 bottomLeft;

    /// <summary>
    /// Reference to <see cref="Maze"/>
    /// </summary>
    private Maze maze;

    [SerializeField]
    [Tooltip("Allow diagonal movement in Pathfinding")]
    private bool allowDiagonal = false;

    /// <summary>
    /// MaxSize Property returning the gridSizeX * gridSizeY
    /// </summary>
    public int MaxSize {
        get
        {
            return gridSizeX * gridSizeY;
        }
    }

    /// <summary>
    /// Awake Method of the PathFindingGrid
    /// </summary>
    private void Awake()
    {
        // Get the reference to the Maze class
        maze = GetComponent<Maze>();

        // Set the nodeDiameter using nodeRadius
        nodeDiameter = nodeRadius * 2;      

        // Add Generate() to the maze.OnBuild event
        maze.OnBuild += Generate;   
    }

    /// <summary>
    /// On <see cref="Maze.OnBuild"/> Generate the Grid for Pathfinding again 
    /// </summary>
    private void Generate()
    {
        StartCoroutine(GenerateGrid());
    }

    /// <summary>
    /// Coroutine to refresh the Grid of Pathfinding Nodes called on <see cref="Maze.OnBuild"/>
    /// </summary>
    private IEnumerator GenerateGrid()
    {
        // Wait 0.1 seconds to allow unwalkable terrain to update completely
        yield return new WaitForSeconds(0.1f);

        // Empty the grid
        grid = null;
        
        // Set the worldSize to the maze size
        worldSize.x = maze.MazeWidth - 1;
        worldSize.y = maze.MazeHeight - 1;

        // Create the gridSize
        gridSizeX = Mathf.RoundToInt(worldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(worldSize.y / nodeDiameter);

        // Create the grid
        CreateGrid();
        yield break;
    }

    /// <summary>
    /// Method for creating the grid of Nodes
    /// </summary>
    private void CreateGrid()
    {
        // Create a new Node array using the gridSizes
        grid = new Node[gridSizeX, gridSizeY];

        // Set the bottomLeft position of the grid
        bottomLeft = transform.position - Vector3.right / 2 - Vector3.forward/ 2;

        // Fill the grid with new Nodes
        for (int i = 0; i < gridSizeX; i++)
            for (int j = 0; j < gridSizeY; j++)
            {
                // Get the position of the new Node
                Vector3 worldPosition = bottomLeft + Vector3.right * (i * nodeDiameter + nodeRadius) + Vector3.forward * (j * nodeDiameter + nodeRadius);

                // Check if the Node is walkable
                bool walkable = !(Physics.CheckSphere(worldPosition, nodeRadius/1.1f, unwalkableLayer));

                // Create the new Node and set it's constructor values
                grid[i,j] = new Node(walkable, worldPosition, i, j);
            }
    }

    /// <summary>
    /// Method that returns a Node based on given position
    /// </summary>
    /// <param name="worldPosition">position to get a Node for</param>
    /// <returns></returns>
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        // Get the localPosition of the worldPosition
        Vector3 localPosition = worldPosition - bottomLeft;

        // Calculate the nearbiest Node
        float percentX = (localPosition.x) / worldSize.x;
        float percentY = (localPosition.z) / worldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        // Round X and Y values with the Clamped percentages
        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

        // Return the closest Node to the worldPosition
        return grid[x, y];
    }

    /// <summary>
    /// Method that returns the neighbours around given Node
    /// </summary>
    /// <param name="node">The Node to find neighbours of</param>
    /// <returns>A List of neighbour Nodes</returns>
    public List<Node> GetNeighbours(Node node)
    {
        // Create a new List of Nodes
        List<Node> neighbours = new List<Node>();

        // If diagonal is allowed
        if (allowDiagonal)
        {
            for (int x = -1; x <= 1; x++)
                for (int y = -1; y <= 1; y++)
                {
                    // Skip the given node x
                    if (x == 0 && y == 0)
                        continue;


                    // Get the nodes surrounding our given node
                    int checkX = node.gridX + x;
                    int checkY = node.gridY + y;

                    // If this neighbour fits within the grid
                    if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                        neighbours.Add(grid[checkX, checkY]);
                }
        }
        // If diagonal is not allowed
        else
        {
            for (int x = -1; x <= 1; x++)
            {
                // Skip x is our given node
                if (x == 0)
                    continue;

                // Get the node surrounding our given node on the X axis
                int checkX = node.gridX + x;

                // Check if the node is within the X boundaries of our grid
                if (checkX >= 0 && checkX < gridSizeX)
                    neighbours.Add(grid[checkX, node.gridY]);
            }

            for (int y = -1; y <= 1; y++)
            {
                // skip if y is our give node y
                if (y == 0)
                    continue;

                // Get the node surrounding our given node on the Y axis
                int checkY = node.gridY + y;

                // Check if the node is within the Y boundaries of our grid
                if (checkY >= 0 && checkY < gridSizeY)
                    neighbours.Add(grid[node.gridX, checkY]);
            }
        }

        return neighbours;
    }

    /// <summary>
    /// OnDrawGizmos for showing the grid in the Scene view
    /// </summary>
    private void OnDrawGizmos()
    {
        // If the grid exists and the displayGridGizmos is enabled
        if (grid != null && displayGridGizmos)
            
            // Draw a cube for eachNode with the color of the walkable value
            foreach (Node node in grid)
            {
                Gizmos.color = (node.walkable) ? Color.green : Color.red;
                Gizmos.DrawCube(node.position, Vector3.one * (nodeDiameter - .1f));
            }
    }
}
