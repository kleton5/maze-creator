﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Class used for returning paths to given target
/// </summary>
public class Pathfinder : MonoBehaviour
{
    /// <summary>
    /// Reference to <see cref="PathRequest"/>
    /// </summary>
    private PathRequest requestManger;

    /// <summary>
    /// Reference to <see cref="PathfindingGrid"/>
    /// </summary>
    private PathfindingGrid grid;

    /// <summary>
    /// Awake method for receiving reference of this gameObject
    /// </summary>
    private void Awake()
    {
        requestManger = GetComponent<PathRequest>();
        grid = GetComponent<PathfindingGrid>();
    }

    /// <summary>
    /// Public method for requesting a path towards destination
    /// </summary>
    /// <param name="start">Current location of requesting gameObject</param>
    /// <param name="destination">Destination location</param>
    public void StartFindPath(Vector3 start, Vector3 destination)
    {
        // Get the path towards the location
        StartCoroutine(GetPath(start, destination));
    }

    /// <summary>
    /// Coroutine for calculation path towards destination
    /// </summary>
    /// <param name="start">The start point</param>
    /// <param name="destination">The destination point</param>
    /// <returns></returns>
    private IEnumerator GetPath(Vector3 start, Vector3 destination)
    {
        // Create a new array of waypoints
        Vector3[] wayPoints = new Vector3[0];
        
        // Create a bool for whether the path is found or not
        bool pathFound = false;

        // Get the nodes for the start and destination positions
        Node startNode = grid.NodeFromWorldPoint(start);
        Node destinationNode = grid.NodeFromWorldPoint(destination);

        if (startNode != null || destinationNode != null)
        {
            // If both the nodes are walkable start calculating path
            if (startNode.walkable && destinationNode.walkable)
            {
                // Create a new Heap of Nodes
                Heap<Node> openSet = new Heap<Node>(grid.MaxSize);

                // Create a HashSet for passed nodes
                HashSet<Node> closedSet = new HashSet<Node>();

                // Add the startNode to the Heap
                openSet.Add(startNode);

                // While there is nodes remaining in the openSet
                while (openSet.ItemCount > 0)
                {
                    // Remove the node from the Heap
                    Node currentNode = openSet.RemoveFirst();

                    // Put the removed node in the HashSet
                    closedSet.Add(currentNode);

                    // If this node is the destinationNode complete the while
                    if (currentNode == destinationNode)
                    {
                        pathFound = true;
                        break;
                    }

                    // Find all neighbours if this node
                    foreach (Node neighbour in grid.GetNeighbours(currentNode))
                    {
                        // Check if these are walkable or already passed
                        if (!neighbour.walkable || closedSet.Contains(neighbour))
                            continue;

                        // Calculate cost of the neigbour node
                        int newMoveCost = currentNode.gCost + GetDistance(currentNode, neighbour);

                        // If our currentNode cost is less than the neighbour cost
                        if (newMoveCost < neighbour.gCost || !openSet.Contains(neighbour))
                        {
                            // Set neighbour Cost values of the neighbour
                            neighbour.gCost = newMoveCost;
                            neighbour.hCost = GetDistance(neighbour, destinationNode);

                            // Set the parent of this node to the curretnNode
                            neighbour.parent = currentNode;

                            // Add this neighbour to the openSet otherwise update the Cost (because a better path was found)
                            if (!openSet.Contains(neighbour))
                                openSet.Add(neighbour);
                            else
                                openSet.UpdateItem(neighbour);
                        }
                    }
                }
            }
        }
        // Wait one frame
        yield return null;
        
        // If the path was found get the waypoints for the path
            if (pathFound)
                wayPoints = RetracePath(startNode, destinationNode);
            
            // Return the result to the PathRequest class
            requestManger.FinishedProcessingPath(wayPoints, pathFound);
    }

    /// <summary>
    /// Method for Retracing the path calculated in <see cref="GetPath(Vector3, Vector3)"/>
    /// </summary>
    /// <param name="start"> The start Node</param>
    /// <param name="destination">The destination Node</param>
    /// <returns></returns>
    private Vector3[] RetracePath(Node start, Node destination)
    {
        // A new list of Nodes for the path
        List<Node> path = new List<Node>();

        // Our currentNode is the destination
        Node currentNode = destination;

        // As long as our currentNode is not the startNode
        while (currentNode != start)
        {
            // Add the currentNode to our path
            path.Add(currentNode);

            // Our next node is the parent of the currentNode set in the GetPath() Coroutine
            currentNode = currentNode.parent;
        }

        // Get the worldPositions of the nodes
        Vector3[] wayPoints = SimplifyPath(path);

        // Reverse the path back from destination to start towards start to destination
        Array.Reverse(wayPoints);

        return wayPoints;
    }

    /// <summary>
    /// Method for turning Nodes path into Vector3 path
    /// </summary>
    /// <param name="path">The Nodes list</param>
    /// <returns></returns>
    private Vector3[] SimplifyPath(List<Node> path)
    {
        // List of waypoints positions
        List<Vector3> wayPoints = new List<Vector3>();

        // Last direction of the waypoint
        Vector2 previousDirection = Vector2.zero;

        // For each Node in the given path
        for (int i = 1; i < path.Count; i++)
        {
            // Get the direction of the Node
            Vector2 newDirection = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);

            // If the newDirection is not the same as the last direction add the wayPoints to the wayPoints List
            if (newDirection != previousDirection)
            {
                // Add the waypoint before the turn (for smoothing)
                wayPoints.Add(path[i - 1].position);

                // Add the waypoint where the direction changed
                wayPoints.Add(path[i].position);
            }
            else
            {
                // Remove the last wayPoint because its not required for the path
                wayPoints.RemoveAt(wayPoints.Count - 1);

                // Add our current Waypoint
                wayPoints.Add(path[i].position);
            }

            // Set the previousDirection to the calculated newDirection
            previousDirection = newDirection;
        }
        
        // return the wayPoints list as an array
        return wayPoints.ToArray();
    }

    /// <summary>
    /// Method for getting the distance between two Nodes
    /// </summary>
    /// <param name="a">The first Node</param>
    /// <param name="b">The second Node</param>
    /// <returns></returns>
    private int GetDistance(Node a, Node b)
    {
        // Get the xDistance between the a and b Node
        int xDistance = Mathf.Abs(a.gridX - b.gridX);

        // Get the yDistance between the a and b Node
        int yDistance = Mathf.Abs(a.gridY - b.gridY);

        // Return the distnace between the nodes based on result
        if (xDistance > yDistance)
            return 14 * yDistance + 10 * (xDistance - yDistance);
        return 14 * xDistance + 10 * (yDistance - xDistance);
    }
}
