﻿using UnityEngine;

/// <summary>
/// Class for Nodes used in the <see cref="PathfindingGrid"/>
/// Node is a IHeapItem for use in the Heap sorting class
/// </summary>
public class Node : IHeapItem<Node>
{
    public bool walkable;

    // The world position of the node
    public Vector3 position;

    // The [,] position of the node
    public int gridX;
    public int gridY;

    // The A* costs of the node
    public int gCost;
    public int hCost;

    // The HeapIndex for reference with the Heap class
    public int HeapIndex { get; set;}

    // The parent node of this node used for retracing paths
    public Node parent;

    /// <summary>
    /// Property containing the total cost of this node
    /// </summary>
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }

    /// <summary>
    /// IComparable implementation for comparing nodes 
    /// </summary>
    /// <param name="compareNode">Node to compare with</param>
    /// <returns></returns>
    public int CompareTo(Node compareNode)
    {
        // Compare the total cost of this node to the compareNode
        int compare = fCost.CompareTo(compareNode.fCost);

        // If they have an equal cost look at the hCost as deciding factor
        if (compare == 0)
            compare = hCost.CompareTo(compareNode.hCost);

        // Return the result reversed
        return -compare;

    }

    /// <summary>
    /// Node class constructor
    /// </summary>
    /// <param name="walkable">Is this node walkable</param>
    /// <param name="position">The world position of the node</param>
    /// <param name="x">The row of the [,] array</param>
    /// <param name="y">The collum of the [,] array</param>
    public Node(bool walkable, Vector3 position, int x, int y)
    {
        this.walkable = walkable;
        this.position = position;
        gridX = x;
        gridY = y;
    }


}
