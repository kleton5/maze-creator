﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Player : MonoBehaviour
{
    private Rigidbody rigid;

    [Range(0,2000)]
    [SerializeField]
    private float movementSpeed = 20;

    [Range(0, 2000)]
    [SerializeField]
    private float povMovementSpeed = 10;

    private bool pov;

    private Enemy[] enemies;

	private void Start ()
    {
        rigid = GetComponent<Rigidbody>();
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            pov = !pov;
            Camera.main.GetComponent<FitCamera>().ChangePOV();
            GetComponentInChildren<MeshRenderer>().enabled = !GetComponentInChildren<MeshRenderer>().enabled;
        }
    }

    private void FixedUpdate()
    {
        if (!pov)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            rigid.velocity = new Vector3
                (Mathf.Lerp(0, (horizontal * movementSpeed * Time.deltaTime), 0.8f), 0, Mathf.Lerp(0, (vertical * movementSpeed * Time.deltaTime), 0.8f));
        }

        else
        {
            rigid.velocity = Vector3.zero;
            float vertical = Input.GetAxis("Vertical");     
            if(vertical > 0.1f)      
                rigid.velocity = Camera.main.transform.forward * povMovementSpeed * Time.deltaTime;
            else if(vertical < -0.1f)
                rigid.velocity = -Camera.main.transform.forward * povMovementSpeed * Time.deltaTime;
        }
    }
}
